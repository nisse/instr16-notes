# 10 years of instr16

Question: Can one design a reasonable instruction set with 16-bit
opcodes?

## Goals

	- 16 general purpose (64-bit) registers (unlike ARM thumb).
	- Practically useable instruction set, biased towards bignum and crypto.
	- Concise, it's no use if one needs twice as many instruction as
	  on a random RISC architecture with 32-bit opcodes.

## Design challenge

	- With three-operand opcodes, dst = s1 OP s2, we'd have space for
	  only 16 operations.

	- Want indexed load/store, which needs three register
	  operands, and load/store with offset, which needs two registers
	  and an immediate constant.

	- No space for variants like load 16-bit value with zero extension or
	  sign extension. Always load and store aligned 64 bits.

	- Want long shift (shifting in bits from a third register), for
	  efficient handling of unaligned data.

## Registers

16 registers as promised, labeled r0 to r15. Not entirely
general purpose: there are some with special usage.

	- r15 is the pc. Used for pc-relative addressing, an it can also
	  be used as the destination for instructions such add and
	  conditional mov.

	- r14 is the link register for subroutine calls (i.e., jsr copies r15
	  to r14 for use as the return address).

	- r8 is special in that it is used by bnz (branch if not zero).

	- r0 - r7 are the only registers usable for float (ieee double)
	  operations, since (i) one can't use *all* registers for floating
	  point anyway, and limiting to 7 saves opcode space, in particular
	  for fused multiply add.

	- r13 can conventionally be used as the stack pointer, but that's an
	  ABI issue, not an ISA issue.

	- Most instructions will have two operands, both reading and
	  updating the dst register.

## Shoehorning

Many tricks are used to make best use of availale opcode space. One
could possibly get space for more instructions by reducing the number
of bits allocated for the prefix instruction and branch offsets.

	- Indexed load and store, ld/st rD [rN, rI] share the same opcode,
	  and load or store is determined by numerical register order, N < I
	  or N > I (and using N = I is invalid).

	- Immediate operands: Use 4-bit field representing one of the 16
	  special values 1,2,3,4,5,6,7,8,10,12,14,16,20,24,32, and a separate
	  sign bit. Same used for load and store with offset.

	- For larger immediates, a special prefix instruction and prefix
	  register. Arguably make instructions variable size, but it can be
	  decoded as a separate instruction.

	- Branch instructions use larger pc-relative immediate offset, 9 bits
	  + sign, and can also use prefix if active.

	- Since there's not space for many branch instructions, only a single
	  condition flag. There's jmp, jsr, bt (branch if true), bf, and also
	  bnz (branch if r8 is non-zero, intended for loops that want
	  condition flag to live between iterations).

	- For shifts with non-constant shift count, including long shift,
	  no variants for left/right or logical/arithmetic, to save opcode bits.
	  But there are plenty of bits in the shift count operand (a 64-bit
	  register). So use the top bits to decide: 00... for left, 01... for
	  logical right, and 1... for arithmetic right.

### Stealing encodings

Some variants of register or immediate constants are redundant or
deemed useless. Those encodings can then be used for other purposes.
E.g.,

	- One can't use the pc as the third register for long shift,
	  instead that encoding is used for the xshift instruction, shifting in the
	  condition flag.

	- `lshift r, #0` is useless no-op. Encoding reused for `clz r`.

	- `cmpugeq r, #-1` is redundant, since it is equvalent to `cmpeq
	  r, #~1`. Encoding reused for `cmsgeq r, #0`.

	- `cmpugeq r, #-8` is redundant, since it is equivalent to `tst
	  r, #-8. Encoding reused for `cmpugt r, #8`.

## Small data access

	- Load/store always access memory as an array of 64-bit
	  words. The low address bits are not present at all on the memory bus.

	- Instead, accessing unaligned addresses triggers a rotation hack:
	  the low address bits are used to rotate the word loded or
	  stored, to emulate a byte-addressed big endian machine. The word
	  is rotated so that the pointed to byte always is in the most
	  significant 8 bits of the register. So load followed by logical
	  or arithmetic left shift by 56 bits produces the right byte, and
	  similarly for naturally align 16-bit and 32-bit values.

	- Storing a byte needs load + injt8 + store, with special
	  two-operand instructions injt8, that copies the least significant
	  8 bits from the src register to the most significant 8 bits of the
	  dst register, leaving the rest of that register unchange. There's
	  also injt16 and injt32.

## Implementation:

	- Assembler (2741 lines)
	- Simulator (1563 lines)
	- Verilog implementation (2623 lines for cpu proper)

### Tools

	- yosys
	- next-pnr
	- icestorm

## Next?

	- add32 instruction, needed for chacha, and other simd-like instructions.
	- System instructions, there's a draft design but not implemented.
	- Floating point implementation.
	- Super-scalar implementation, e.g, decoding a full 64-bit word at
	  a time (4 instructions), and an internal register bank (accessed
	  via register renaming) of 32 registers. Needs a larger fpga than
	  ice40, though.

## Debugging

### Many levels of bugs

	- Software bugs, e.g., register clobber.

	- Processor implementation bugs, e.g., broken handshake on
	  internal wishbone buses.

	- Tool bugs, e.g., improper setting of configuration bits
	  controlling on which clock flank registers are read and written.

	- FPGA bugs, e.g., ice40 block RAM not properly initialized until
	  3µs after reset.

### Tools

	- Trace output from simulator and verilog simulation + adhoc printf
	  debugging.

	- Logic analyzer, using ramaining block RAM to capture signals on
	  the FPGA and dump over the serial port.
